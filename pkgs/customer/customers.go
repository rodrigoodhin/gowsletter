package customer

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/rodrigoodhin/gowsletter/pkgs/handlers"
)

// listCustomers
// @Summary list customers
// @Description retrieves all customers
// @Tags customer
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /customer [get]
func (d data) listCustomers(c *fiber.Ctx) (err error) {
	var customers []Customer

	if customers, err = d.listCustomersQuery(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find Customers",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"customers": customers},
	}.Send()
}

// getCustomer
// @Summary get customer
// @Description retrieves a customer
// @Tags customer
// @Produce json
// @Param id path string true "Customer ID"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /customer/{id} [get]
func (d data) getCustomer(c *fiber.Ctx) (err error) {
	var customer Customer

	if customer, err = d.getCustomerQuery(c.Params("email")); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find Customer",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"customer": customer},
	}.Send()
}

// upsertCustomer
// @Summary add or update customer
// @Description adds or updates a customer
// @Tags customer
// @Accept json
// @Produce json
// @Param message body models.CustomerUpsert true "Customer Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /customer [put]
func (d data) upsertCustomer(c *fiber.Ctx) (err error) {
	var (
		customer       Customer
		customerUpsert Customer
	)

	if err = c.BodyParser(&customerUpsert); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to parse request body",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	if err = customerUpsert.Validate(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to validate Customer",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	if customer, err = d.upsertCustomerQuery(customerUpsert); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to upsert Customer",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"customer": customer},
	}.Send()
}

// deleteCustomer
// @Summary delete customer
// @Description deletes a customer
// @Tags customer
// @Produce json
// @Param id path string true "Customer ID"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /customer/{id} [delete]
func (d data) deleteCustomer(c *fiber.Ctx) (err error) {
	if err = d.deleteCustomerQuery(c.Params("email")); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to delete Customer",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
	}.Send()
}
