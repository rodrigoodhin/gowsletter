package customer

import (
	"github.com/go-ozzo/ozzo-validation/is"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/rodrigoodhin/gowsletter/internal/db"
)

var EmailRule = []validation.Rule{
	validation.Required,
	is.Email,
	validation.Required.Error("Must be a valid Email"),
}

// Customer - Customer table model
type Customer struct {
	db.Model
	Email  string `json:"email" bun:",unique"`
	Status bool   `json:"status"`
}

func (a Customer) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Email, EmailRule...),
	)
}
