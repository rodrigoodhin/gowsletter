package customer

import (
	"fmt"
)

func (d data) migrate() (err error) {

	if _, err = d.DB.NewCreateTable().Model(&Customer{}).IfNotExists().Exec(d.Ctx); err != nil {
		return
	}

	return
}

//func (d data) pkQuery(email string) (customer models.Customer, err error) {
//	customer.ID, err = uuid.Parse(customerId)
//	if err != nil {
//		return
//	}
//
//	err = d.DB.NewSelect().
//		Model(&customer).
//		WherePK().
//		Scan(d.Ctx, &customer)
//
//	return
//}

func (d data) listCustomersQuery() (customers []Customer, err error) {
	err = d.DB.NewSelect().
		Model(&Customer{}).
		Scan(d.Ctx, &customers)

	return
}

func (d data) getCustomerQuery(email string) (customer Customer, err error) {
	customer.Email = email

	err = d.DB.NewSelect().
		Model(&customer).
		Where("email = ?", email).
		Scan(d.Ctx, &customer)

	return
}

func (d data) upsertCustomerQuery(customerUpsert Customer) (customer Customer, err error) {
	customer.Email = customerUpsert.Email
	customer.Status = customerUpsert.Status

	_, err = d.DB.NewInsert().
		Model(&customer).
		On("CONFLICT (email) DO UPDATE").
		Exec(d.Ctx)

	return
}

func (d data) deleteCustomerQuery(email string) (err error) {
	var customer Customer

	customer.Email = email

	res, err := d.DB.NewDelete().
		Model(&customer).
		Where("email = ?", email).
		Exec(d.Ctx)
	if err != nil {
		return
	}

	rows, err := res.RowsAffected()
	if rows == 0 || err != nil {
		return fmt.Errorf("record not found")
	}

	return
}
