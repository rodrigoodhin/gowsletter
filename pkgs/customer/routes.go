package customer

import (
	"github.com/gofiber/fiber/v2"
)

func (d data) customer(router fiber.Router) fiber.Router {

	router.Get("/", d.listCustomers)

	router.Get("/:email", d.getCustomer)

	router.Put("/", d.upsertCustomer)

	router.Delete("/:email", d.deleteCustomer)

	return router
}
