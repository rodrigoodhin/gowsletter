package customer

import (
	"context"

	"github.com/uptrace/bun"
	"gitlab.com/rodrigoodhin/gowsletter/internal/app"
)

type data struct {
	DB  *bun.DB
	Ctx context.Context
}

func Init(App *app.Data) (err error) {
	d := &data{
		DB:  App.DB,
		Ctx: context.Background(),
	}

	// Auto Migrate
	err = d.migrate()
	if err != nil {
		return
	}

	// Add routers
	d.customer(App.Api.Group("customer"))

	return nil
}
