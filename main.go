package main

import (
	"fmt"
	"log"
	"os"

	swagger "github.com/arsmn/fiber-swagger/v2"
	//1_ "gitlab.com/rodrigoodhin/gowsletter/docs"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/monitor"
	"github.com/gofiber/fiber/v2/middleware/requestid"
	"github.com/spf13/viper"
	"gitlab.com/rodrigoodhin/gowsletter/internal/app"
	"gitlab.com/rodrigoodhin/gowsletter/internal/db"
	"gitlab.com/rodrigoodhin/gowsletter/pkgs/customer"
)

var App = &app.Data{}

// @title gowsletter API
// @version 1.0
// @description Swagger for gowsletter API
// @contact.name gowsletter Support
// @contact.email contact@gowsletter.io
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @host localhost:7000
// @BasePath /
func main() {
	// Setup a fiber service
	service := Setup()

	if err := service.Listen(":7000"); err != nil {
		log.Fatalln(err.Error())
	}
}

// Setup - setup a fiber service with all of its routes
func Setup() *fiber.App {
	var err error

	// Init Config
	loadConfig()

	// Init DB
	App.DB = db.Init()

	// Init API Service
	service := fiber.New()

	// Default middleware config
	service.Use(requestid.New())

	// Add swagger
	service.Get("/swagger/*", swagger.New(swagger.Config{ // custom
		URL:          "/swagger/doc.json",
		DeepLinking:  false,
		DocExpansion: "none",
	}))

	// Service Logger
	if viper.GetString("logger.logLevel") != "Silent" {
		service.Use(logger.New(logger.Config{
			TimeZone: "Europe/Lisbon",
		}))
	}

	// Cors
	service.Use(cors.New(cors.Config{
		AllowOrigins:     "*",
		AllowHeaders:     "Origin, Content-Type, Accept, X-Requested-With, Authorization, sentry-trace",
		AllowCredentials: true,
	}))

	// Set index route
	service.Get("/", func(c *fiber.Ctx) error {
		return c.SendString(viper.GetString("app") + " 👋!")
	})

	// Set security headers
	service.Use(func(c *fiber.Ctx) error {
		// Set some security headers:
		c.Set("X-XSS-Protection", "1; mode=block")
		c.Set("X-Content-Type-Options", "nosniff")
		c.Set("X-Download-Options", "noopen")
		c.Set("Strict-Transport-Security", "max-age=5184000")
		c.Set("X-Frame-Options", "SAMEORIGIN")
		c.Set("X-DNS-Prefetch-Control", "off")
		// Go to next middleware:
		return c.Next()
	})

	// Add Fiber Monitor
	service.Get(viper.GetString("monitoring.monitorPath"), monitor.New())

	// Create API route group
	App.Api = service.Group("api")

	// Add Customer Package
	if err = customer.Init(App); err != nil {
		log.Fatalln(err.Error())
	}

	// Return the configured service
	return service
}

func loadConfig() {
	var env string
	if os.Getenv("ENV") != "" {
		env = os.Getenv("ENV")
	} else {
		env = "local"
	}
	viper.SetConfigName(env)
	viper.SetConfigType("toml")
	viper.AddConfigPath("config/")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %w \n", err))
	}

}
