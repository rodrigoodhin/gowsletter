package app

import (
	"github.com/gofiber/fiber/v2"
	"github.com/uptrace/bun"
)

type Data struct {
	DB         *bun.DB
	Api        fiber.Router
	Static     fiber.Router
}
