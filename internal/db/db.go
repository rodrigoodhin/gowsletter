package db

import (
	"database/sql"
	"log"
	"os"
	"time"

	"github.com/uptrace/bun/extra/bundebug"

	"github.com/google/uuid"
	"github.com/spf13/viper"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"
)

var (
	err   error
	db    *bun.DB
	sqldb *sql.DB
)

func Init() *bun.DB {
	dsn := "postgres://" + os.Getenv("POSTGRES_USER") +
		":" + os.Getenv("POSTGRES_PASSWORD") +
		"@" + os.Getenv("POSTGRES_HOST") +
		":5432/" + os.Getenv("POSTGRES_DB") +
		"?sslmode=disable"
	sqldb = sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(dsn)))
	db = bun.NewDB(sqldb, pgdialect.New())
	db.AddQueryHook(setLog())

	if err != nil {
		log.Panicf("failed to connect database: %s", err.Error())
	}

	return db
}

type Model struct {
	ID        uuid.UUID `bun:",pk,type:uuid,default:uuid_generate_v4()"`
	CreatedAt time.Time `bun:",nullzero,notnull,default:current_timestamp"`
	UpdatedAt time.Time `bun:",nullzero,notnull,default:current_timestamp"`
	DeletedAt time.Time `bun:",soft_delete"`
}

// setLog - Configure db log
func setLog() *bundebug.QueryHook {
	switch viper.GetString("logger.logLevel") {
	case "Silent":
		return bundebug.NewQueryHook(bundebug.WithEnabled(false), bundebug.FromEnv(""))
	case "Info":
		return bundebug.NewQueryHook(bundebug.WithVerbose(true))
	case "Error", "Warn":
		return bundebug.NewQueryHook()
	}

	return nil
}